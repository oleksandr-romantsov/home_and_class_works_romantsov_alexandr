const render_play = song =>{
const MusicPlaying = document.getElementById( 'MusicPlaying' );

let node = `
	<div class="song__name">
  	${song.name}        
  </div>
  <div class="song__creator">
    ${song.creator}
  </div>
  <div class="song__duration">
    ${song.time[ 0 ]}:${song.time[ 1 ]}
  </div>
`
MusicPlaying.innerHTML = node;
}
export default render_play;