let target = document.getElementById( 'target' );
/*

document
querySelector
constructor
extends
addEventListener
getElementById
Math
round
random
return
class
console.log
classList.add
classList.remove
appendChild
const
let
var
innerHTML

*/


class SuperDude {
  constructor( name, arr__super__powers ){
    this.name = name;
    this.arr__super__powers = arr__super__powers;
    this.set_property.call( this );
    // console.log( this );

    //
    delete this.set_property;
    delete this.arr__super__powers;
  }
  set_property(){
    this.arr__super__powers.map( ( item ) => {
      this[item.name] = item.spell.bind( item );
    });
  }
}

let superPowers = [
  { name:'Invisibility', spell: function(){ return `${this.name} hide from you`} },
  { name:'superSpeed', spell: function(){ return `${this.name} running from you`} },
  { name:'superSight', spell: function(){ return `${this.name} see you`} },
  { name:'superFroze', spell: function(){ return `${this.name} will froze you`} },
  { name:'superSkin', spell: function(){ return `${this.name} skin is unbreakable`} },
];
let Luther = new SuperDude( 'Bob', superPowers );

for ( let key in Luther ){
  if( key !== 'name' ){
    target.innerHTML += `<b>${Luther[key]()}</b><br />`;
  }
}
