/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/imports.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/imports.js":
/*!********************************!*\
  !*** ./application/imports.js ***!
  \********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _singleton__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./singleton */ \"./application/singleton/index.js\");\n/* harmony import */ var _classworks_objectfreeze__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../classworks/objectfreeze */ \"./classworks/objectfreeze.js\");\n  /*\n    import defaultExport from \"module-name\"; <- node_modules\n    import * as name from \"module-name\";\n    import { export } from \"module-name\";\n    import { export as alias } from \"module-name\";\n    import { export1 , export2 } from \"module-name\";\n    import { export1 , export2 as alias2 } from \"module-name\";\n    import defaultExport, * as name from \"module-name\";\n    import \"module-name\";\n  */\n  /*\n    import defaultExport from \"module-name\";\n  */\n\n\n\n\n\nObject(_classworks_objectfreeze__WEBPACK_IMPORTED_MODULE_1__[\"default\"])();\n\n//# sourceURL=webpack:///./application/imports.js?");

/***/ }),

/***/ "./application/singleton/index.js":
/*!****************************************!*\
  !*** ./application/singleton/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _object_freeze__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./object_freeze */ \"./application/singleton/object_freeze.js\");\n/* harmony import */ var _old_singleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./old-singleton */ \"./application/singleton/old-singleton.js\");\n/* harmony import */ var _new_singleton__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./new-singleton */ \"./application/singleton/new-singleton.js\");\n\n\n\n// FreezeDemo();\n\nconst SingletonDemo = () => {\n  /*\n    Singleton (он же одиночка)— это паттерн проектирования,\n    который гарантирует, что у класса есть только один экземпляр,\n    и предоставляет к нему глобальную точку доступа.\n\n    Про паттерн: https://refactoring.guru/ru/design-patterns/singleton\n  */\n\n  // Посмотрим на две реализаци старую - до ES6\n  // oldSingletonDemo();\n  // И новую\n  newSingetonDemo();\n};\n\nconst oldSingletonDemo = () => {\n//      // Смотрим реализацию в файле old-singleton.js  \n\n    console.log( _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"] );\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 0, language: 'js'});\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 1, language: 'phyton'});\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 2, language: 'php'});\n    _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].add({id: 3, language: 'ruby'});\n\n    console.log('OldSingleton', _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"] );\n    let myLang = _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].get(0);\n    let myLang2 = _old_singleton__WEBPACK_IMPORTED_MODULE_1__[\"default\"].get(3);\n    console.log('OldSingleton -> myLang', myLang);\n    console.log('OldSingleton -> myLang', myLang2);\n};\n//\nconst newSingetonDemo = () => {\n  // Как и все в js в 2017-18 меньше, быстрее, чище!\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 0, language: 'js'});\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 1, language: 'phyton'});\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 2, language: 'php'});\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].add({id: 3, language: 'ruby'});\n  //\n  console.log('NewSingleton', _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"]);\n  let myLang = _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].get(0);\n  console.log('NewSingleton -> myLang', myLang);\n\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].addCounter(20);\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].addCounter(20);\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].addCounter(20);\n  _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].addCounter(20);\n  console.log( _new_singleton__WEBPACK_IMPORTED_MODULE_2__[\"default\"].getCounter() );\n\n  /*\n    Демо усложнить синглтон\n  */\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (SingletonDemo);\n\n\n//# sourceURL=webpack:///./application/singleton/index.js?");

/***/ }),

/***/ "./application/singleton/new-singleton.js":
/*!************************************************!*\
  !*** ./application/singleton/new-singleton.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// Так как является константой, не может быть измененно\nconst _data = {\n  store: [],\n  counter: 0\n};\n\n// Создаем обьект и методы\nconst Store = {\n  add: item => _data.store.push(item),\n  get: id => _data.store.find( d => d.id === id ),\n  showAllLang: () => [ ..._data.store ],\n  getCounter: () => _data.counter,\n  addCounter: (num) => _data.counter += num\n};\n// Замораживаем\nObject.freeze(Store);\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Store);\n\n\n//# sourceURL=webpack:///./application/singleton/new-singleton.js?");

/***/ }),

/***/ "./application/singleton/object_freeze.js":
/*!************************************************!*\
  !*** ./application/singleton/object_freeze.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst ObjectFreezeDemo = () => {\n  /*\n    Разберемся вначале с Object.freeze. ->\n    https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze\n  */\n\n  const myObj = {\n    name: 'Dexter',\n    prop: () => {\n      console.log(`${undefined.name} woohoo!`);\n    }\n  };\n  console.log( myObj );\n  myObj.name = \"Debra\";\n  console.log( myObj );\n\n  /*\n    Заморозка обьекта, это необратимый процесс.\n    Единожды заомроженный обьект уже не может быть разморожен!\n    Заморозим обьект\n  */\n  // let frozen = Object.freeze(myObj);\n  //Попробуем изменить или добавить значение\n  // myObj.name = 'name';\n  // frozen.name = \"Vince\";\n  // frozen.secondName = 'Morgan';\n\n  // Проверим сам обьект и его идентичность с тем,\n  // что мы создали в самом начале функции\n  // console.log( 'frozen', frozen, 'equal?', frozen === myObj);\n\n  /*\n    Так же, метод работает для массивов\n  */\n  // let frozenArray = Object.freeze(['froze', 'inside', 'of', 'array']);\n\n  // Попробуем добавить новое значение\n  // frozenArray[0] = 'Noooooo!';\n\n  // Попробуем использовать методы\n  // let sliceOfColdAndSadArray = frozenArray.slice(0, 1);\n  // sliceOfColdAndSadArray.map( item => console.log( item ) );\n  // console.log(frozenArray, sliceOfColdAndSadArray);\n\n  // Метоы для проверки\n  // Object.isFrozen( obj ) -> Вернет true если обьект заморожен\n  // console.log(\n  //   'is myObj frozen?', Object.isFrozen( myObj ),\n  //   '\\nis frozen frozen?', Object.isFrozen( frozen ),\n  //   '\\nis array frozen', Object.isFrozen( frozenArray )\n  // );\n\n  /*\n    Заморозка в обьектах является не глубокой\n  */\n\n  let universe = {\n    infinity: Infinity,\n    good: ['cats', 'love', 'rock-n-roll'],\n    evil: {\n      bonuses: ['cookies', 'great look']\n    }\n  };\n//   let x = Object.getOwnPropertyNames(universe);\n//   console.log(x);\n  let frozenUniverse = Object.freeze(universe);\n      // frozenUniverse.humans = [];\n      frozenUniverse.evil.humans = [];\n//\n      console.log(frozenUniverse);\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (ObjectFreezeDemo);\n\n\n//# sourceURL=webpack:///./application/singleton/object_freeze.js?");

/***/ }),

/***/ "./application/singleton/old-singleton.js":
/*!************************************************!*\
  !*** ./application/singleton/old-singleton.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nconst OldSingleton = ( function(){\n var _data = [];\n\n function add(item){\n   _data.push(item);\n }\n\n function showData(){\n   return [..._data];\n }\n\n function get(id){\n   return _data.find((d) => {\n       return d.id === id;\n   });\n }\n\n return {\n   add: add,\n   get: get,\n   showData: showData\n };\n})();\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (OldSingleton);\n\n\n//# sourceURL=webpack:///./application/singleton/old-singleton.js?");

/***/ }),

/***/ "./classworks/objectfreeze.js":
/*!************************************!*\
  !*** ./classworks/objectfreeze.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание: написать функцию, для глубокой заморозки обьектов.\n\n  Обьект для работы:\n  let universe = {\n    infinity: Infinity,\n    good: ['cats', 'love', 'rock-n-roll'],\n    evil: {\n      bonuses: ['cookies', 'great look']\n    }\n  };\n\n  frozenUniverse.evil.humans = []; -> Не должен отработать.\n\n  Методы для работы:\n  1. Object.getOwnPropertyNames(obj);\n      -> Получаем имена свойств из объекта obj в виде массива\n\n  2. Проверка через typeof на обьект или !== null\n  if (typeof prop == 'object' && prop !== null){...}\n\n  Тестирование:\n\n  let FarGalaxy = DeepFreeze(universe);\n      FarGalaxy.good.push('javascript'); // false\n      FarGalaxy.something = 'Wow!'; // false\n      FarGalaxy.evil.humans = [];   // false\n\n*/\n\nconst work = () => {\n  console.log( 'your code ');\n\n\n\n\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (work);\n\n\n//# sourceURL=webpack:///./classworks/objectfreeze.js?");

/***/ })

/******/ });