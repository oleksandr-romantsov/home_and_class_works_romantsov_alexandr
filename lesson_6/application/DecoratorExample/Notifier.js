class Notifier {
  send( msg, baseNode, block ){
      console.log('CLASS NOTIFIER: mesasge was sended:', msg );
      const target = baseNode.querySelector(`.notifier__item[data-slug="${block}"]`);
      console.log('target', target);
      target.innerHTML += `<div>${msg}</div>`;
  }
}


/*

  // Пока не смотрит вниз :)


















*/


class Application{
  constructor(){
    this.notifierTargets = [
      {name: 'sms', image: 'images/sms.svg'},
      {name: 'mail', image: 'images/gmail.svg'},
      {name: 'telegram', image: 'images/telegram.svg'},
      {name: 'viber', image: 'images/viber.svg'},
      {name: 'slack', image: 'images/slack.svg'},
    ];
    this.notifier = new Notifier( this.notifierTargets );
    this.createInterface = this.createInterface.bind( this );
    this.node = null;
  }

  createInterface(){
    const root = document.getElementById( 'root' );
    const AppNode = document.createElement( 'section' );

    AppNode.className = 'notifer_app';
      console.log( this.notifierTargets );
    AppNode.innerHTML =
    `
      <div class="notifer_app--container">
        <header>
          <input class="notifier__messanger" type="text"/>
          <button class="notifier__send">Send Message</button>
        </header>
        <div class="notifier__container">
        ${
          this.notifierTargets.map( item =>
            `
            <div class="notifier__item" data-slug="${item.name}">
              <header class="notifier__header">
                <img width="25" src="${item.image}"/>
                <span>${item.name}</span>
              </header>
              <div class="notifier__messages"></div>
            </div>
            `).join('')
        }
        </div>
      </div>
    `;
    const container = AppNode.querySelector( '.notifier__container' );
    const btn = AppNode.querySelector( '.notifier__send' );
    const input = AppNode.querySelector( '.notifier__messanger' );
    btn.addEventListener( 'click', () => {
      let value = input.value;
      this.notifier.send__message( value, this.node );
    });

    this.node = AppNode;
    root.appendChild( AppNode );
  }

}


  const NotifierApp = new Application();
  
  NotifierApp.createInterface();

  console.log( NotifierApp );






























export class SmsNotifier extends Notifier {
  constructor({ type }){
    super();

    this.type = type;
  }

  send( msg, baseNode, block = 'sms' ){

    if( this.type === 'kyivstar'){
      console.log('send to kyivstar');
    }
    if( this.type === 'mts'){
      console.log('send to mts');
    }
    ///....
    // fetch('kyivstar.ua/send?...')
    super.send(msg, baseNode, block);
  }
}

export class ViberNotifier extends Notifier {
  send( msg, baseNode, block = 'viber'){
    //...
    // fetch('viber.com/send?...')
      super.send(msg, baseNode, block);
  }
}

export class GmailNotifier extends Notifier {
  send( msg, baseNode, block = 'mail' ){
      // fetch('gmail.com/send?...')
      super.send(msg, baseNode, block);
  }
}

export class TelegramNotifier extends Notifier {
  send( msg, baseNode, block = 'telegram' ){
      // fetch('telegram.com/send?...')
      super.send(msg, baseNode, block);
  }
}

export class SlackNotifier extends Notifier {
  send( msg, baseNode, block = 'slack' ){
      // fetch('slack.com/send?...')
      super.send(msg, baseNode, block);
  }
}


export default Notifier;
