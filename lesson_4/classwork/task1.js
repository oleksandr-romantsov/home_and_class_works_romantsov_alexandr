/*
  Composition:

  Задание при помощи композиции создать объекты 4х типов:

  functions:
    - MakeBackendMagic
    - MakeFrontendMagic
    - MakeItLooksBeautiful
    - DistributeTasks
    - DrinkSomeTea
    - WatchYoutube
    - Procrastinate

  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea

  ProjectManager(name,gender,age) => { name, gender, age, type: 'project'}

*/

let create = ( name, gender, age, type )=>{
  const MakeBackendMagic = ()=>{
    console.log( `${name} is makeing backend magic` );
  }
  const MakeFrontendMagic = ()=>{
    console.log( `${name} is makeing frontend magic` );
  }
  const MakeItLooksBeautiful = ()=>{
    console.log( `${name} is makeing it's so beauty` );
  }
  const DistributeTasks = ()=>{
    console.log( `${name} is distributeing tasks` );
  }
  const DrinkSomeTea = ()=>{
    console.log( `${name} is drining some tea` );
  }
  const WatchYoutube = ()=>{
    console.log( `${name} is wathing YouTube` );
  }
  const Procrastinate = ()=>{
    console.log( `${name} is procrastinateing` );
  }
  const res = {
    name,
    gender,
    age,
    type
  }
  if( type === 'backend' ){
    res.MakeBackendMagic = MakeBackendMagic;
    res.DrinkSomeTea = DrinkSomeTea;
    res.Procrastinate = Procrastinate;
  }
  if( type === 'frontend' ){
    res.MakeFrontendMagic = MakeFrontendMagic;
    res.DrinkSomeTea = DrinkSomeTea;
    res.WatchYoutube = WatchYoutube;
  }
  if( type === 'designer' ){
    res.MakeItLooksBeautiful = MakeItLooksBeautiful;
    res.WatchYoutube = WatchYoutube;
    res.Procrastinate = Procrastinate;
  }
  if( type === 'project' ){
    res.DistributeTasks = DistributeTasks;
    res.Procrastinate = Procrastinate;
    res.DrinkSomeTea = DrinkSomeTea;
  }
  return res;
}
// console.log( 'import accept' );
export default create;