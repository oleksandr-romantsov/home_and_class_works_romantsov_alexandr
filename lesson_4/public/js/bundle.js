/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classwork_task2_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classwork/task2.js */ \"./classwork/task2.js\");\n// import comp from './patterns/composition_vs_inheritance';\n// import fabric from './patterns/fabric';\n// comp();\n// fabric();\n\n// import task1 from '../classwork/task1.js';\n// let alex = task1( 'Alex', 'man', 24, 'frontend' );\n// // alex.MakeFrontendMagic();\n// // alex.DrinkSomeTea();\n\n\n\n\nlet data_url = 'http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2';\nObject(_classwork_task2_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])( data_url );\n\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classwork/task1.js":
/*!****************************!*\
  !*** ./classwork/task1.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Composition:\n\n  Задание при помощи композиции создать объекты 4х типов:\n\n  functions:\n    - MakeBackendMagic\n    - MakeFrontendMagic\n    - MakeItLooksBeautiful\n    - DistributeTasks\n    - DrinkSomeTea\n    - WatchYoutube\n    - Procrastinate\n\n  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate\n  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube\n  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate\n  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea\n\n  ProjectManager(name,gender,age) => { name, gender, age, type: 'project'}\n\n*/\n\nlet create = ( name, gender, age, type )=>{\n  const MakeBackendMagic = ()=>{\n    console.log( `${name} is makeing backend magic` );\n  }\n  const MakeFrontendMagic = ()=>{\n    console.log( `${name} is makeing frontend magic` );\n  }\n  const MakeItLooksBeautiful = ()=>{\n    console.log( `${name} is makeing it's so beauty` );\n  }\n  const DistributeTasks = ()=>{\n    console.log( `${name} is distributeing tasks` );\n  }\n  const DrinkSomeTea = ()=>{\n    console.log( `${name} is drining some tea` );\n  }\n  const WatchYoutube = ()=>{\n    console.log( `${name} is wathing YouTube` );\n  }\n  const Procrastinate = ()=>{\n    console.log( `${name} is procrastinateing` );\n  }\n  const res = {\n    name,\n    gender,\n    age,\n    type\n  }\n  if( type === 'backend' ){\n    res.MakeBackendMagic = MakeBackendMagic;\n    res.DrinkSomeTea = DrinkSomeTea;\n    res.Procrastinate = Procrastinate;\n  }\n  if( type === 'frontend' ){\n    res.MakeFrontendMagic = MakeFrontendMagic;\n    res.DrinkSomeTea = DrinkSomeTea;\n    res.WatchYoutube = WatchYoutube;\n  }\n  if( type === 'designer' ){\n    res.MakeItLooksBeautiful = MakeItLooksBeautiful;\n    res.WatchYoutube = WatchYoutube;\n    res.Procrastinate = Procrastinate;\n  }\n  if( type === 'project' ){\n    res.DistributeTasks = DistributeTasks;\n    res.Procrastinate = Procrastinate;\n    res.DrinkSomeTea = DrinkSomeTea;\n  }\n  return res;\n}\n// console.log( 'import accept' );\n/* harmony default export */ __webpack_exports__[\"default\"] = (create);\n\n//# sourceURL=webpack:///./classwork/task1.js?");

/***/ }),

/***/ "./classwork/task2.js":
/*!****************************!*\
  !*** ./classwork/task2.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _task1_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./task1.js */ \"./classwork/task1.js\");\n\n/*\n\n  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет\n  расспределять и создавать сотрудников компании нужного типа.\n\n  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2\n\n\t\n\n  HeadHunt => {\n    hire( obj ){\n      ...\n    }\n  }\n\n  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики\n\n*/\n// console.log(  )\n\n\nconst create = teg =>{\n\treturn document.createElement( teg );\n}\nconst target = document.getElementById( 'target' );\nconst target_2 = document.getElementById( 'target_2' );\n\nasync function get_data( url ){\n\tawait fetch( url ).then( res => {\n\t\t   \treturn res.json()\n\t\t  }).then( data => {\n\t\t  \tlet _temp = new HeadHunt( data );\n\t\t  \t_temp.render();\n\t\t})\n}\n\nclass HeadHunt {\n\tconstructor( data ){\n\t\tthis.data = data;\n\t\tthis.hired = [];\n\n\t\tthis.hire.bind( this );\n\t\tthis.render.bind( this );\n\t}\n\trender(){\n\t\tlet table = document.createElement( 'table' );\n\n\t\tconst glob = this;\n\t\tthis.data.map( function( item, i ) {\n\t\t\tlet { age, name, type } = item;\n\t\t\tlet tr = create( 'tr' )\n\n\t\t\tlet td_name = create( 'td' )\n\t\t\ttd_name.innerHTML = `${name} (${age})`;\n\n\t\t\tlet td_type = create( 'td' )\n\t\t\ttd_type.innerHTML = type;\n\n\t\t\tlet td_bttn = create( 'td' )\n\t\t\tlet button = create( 'button' );\n\t\t\tbutton.addEventListener( 'click', function() { glob.hire.call( glob, i ) });\n\t\t\tbutton.innerHTML = 'Нанять';\n\n\t\t\ttd_bttn.appendChild( button );\n\n\t\t\ttr.appendChild( td_name );\n\t\t\ttr.appendChild( td_type );\n\t\t\ttr.appendChild( td_bttn );\n\t\t\ttable.appendChild( tr );\n\t\t})\n\t\ttarget.innerHTML = null;\n\t\ttarget_2.innerHTML = null;\n\t\ttarget.appendChild( table );\n\t\tlet ul = create( 'ul' );\n\t\tthis.hired.map( function( item ){\n\t\t\tconsole.log( item );\n\t\t\tlet { name, type } = item[ 1 ][ 0 ];\n\t\t\tlet li = create( 'li' );\n\t\t\tli.innerHTML = `${name} [${type}] `;\n\t\t\tul.appendChild( li );\n\t\t});\n\t\ttarget_2.appendChild( ul );\n\t}\n\thire( i ){\n\t\tlet _tmp = [];\n\t\tlet selected = this.data.splice( i, 1 );\n\t\tlet { name, gender, age, type } = selected[ 0 ];\n\t\t_tmp[ 0 ] = Object(_task1_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])( name, gender, age, type );\n\t\t_tmp[ 1 ] = selected;\n\t\tthis.hired.push( _tmp );\n\t\tthis.render();\n\t}\n}\n/* harmony default export */ __webpack_exports__[\"default\"] = (get_data);\n\n//# sourceURL=webpack:///./classwork/task2.js?");

/***/ })

/******/ });