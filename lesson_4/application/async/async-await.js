/*
  ASYNC-AWAIT

  Внутри функции которая помеченая как async в можете поместить
  ключевое слово await перед выражением которое вернет promise.
  Во время выполнения асинхронной функции она останавливается пока
  промис не станет выполненным.

  Задача конструкции async-await писать асинхронный код, который
  будет читаться так же просто как и синхронный

*/

// 1. Возвращает промис
  // async function PlayGame(){
  //   let rand = Math.floor(  Math.random() *  100 );
  //   rand % 2 === 0 ? rand = true : rand = false;
  //   return rand;
  // }
  // let GameResult = PlayGame();
  // console.log( GameResult );

  // GameResult.then(
  //   function( res ){
  //     res === true ?
  //       console.log('YOU WON') :
  //       console.log('YOU LOSE')
  //     }
  //   );

  // Demo with Response Awaiting

  // function resolveAfter( number, ms ) {
  //   return new Promise(
  //     function( resolve ){
  //       setTimeout(() => {
  //         resolve( number );
  //       }, ms);
  //     });
  // }

  // const returnRes = res => res

  // async function CombineNumbers(){
  //   let a = await resolveAfter( 70, 1000 )
  //     .then( (res) => {
  //       console.log( 'first done', res )
  //       return res;
  //     });

  //     // console.log( 'a', a );


  //   let b = await resolveAfter( 40, 3000 )
  //   .then( (res) => {
  //     console.log( 'second done', res );
  //     return res;
  //   });
  //   return a + b;
  // }
  // let sixteen = CombineNumbers();
  //     sixteen.then( res => console.log( 'Final Result', res ));

  // Combine User
  // async function getUserWithFriends(){
    // const getUserResponse = await fetch( "http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2" );
    // const users = await getUserResponse.json();
    
    // получили всех людей
    // console.log( users );
    
    // const selectedUserName = users[ 0 ];
    // Выбрали одного из них
    // console.log(selectedUserName)

    // const getUserFriends = await fetch( "http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2" );
    // const UserFriends = await getUserFriends.json();

    // получили друзей выбраного человека  
    // console.log(UserFriends);

  //   let { age, name, gender } = selectedUserName;
  //   const CombinedUser = {
  //     age,
  //     name,
  //     gender,
  //     friends: UserFriends[ 0 ].friends
  //   };
  //   return CombinedUser;
  // }

  
  // var UserWithFriends = getUserWithFriends();

      // Т. к. UserWithFriends - это промис, то..
      // UserWithFriends.then( data => console.log( 'Final Person:', data ));



// async function my__awesome__func(){

//   const peaples_p = await fetch( "http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2" );
//   const peaples_obj = await peaples_p.json();
//   console.log( peaples_obj );

// }

// my__awesome__func();